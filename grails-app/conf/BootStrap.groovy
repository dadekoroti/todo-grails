import com.security.auth.Role
import com.security.auth.UserRole
import grailstodo.Interest
import org.grails.datastore.mapping.query.Query
import org.springframework.security.core.userdetails.User

class BootStrap {
    def userService

    def init = { servletContext ->

userService.initUser();
    }
    def destroy = {
    }
}
