package grailstodo

import com.security.auth.User

class UserInformation {

    User user;
    String  name;
    String address;
    Date dob;
    Gender gender;

    enum Gender {
        Male('Male'),
        Female('Female'),
        Other('Other')

        final String value

        Gender(String value) {
            this.value = value
        }

        String toString() {
            value
        }

        String getKey() {
            name()
        }
    }
    static constraints = {
    }
}
