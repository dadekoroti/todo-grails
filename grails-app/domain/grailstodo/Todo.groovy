package grailstodo

class Todo {
    String note
    Date created_at
    TodoStatus status
    Date deadline

    static hasMany = [tag: Tag]
    static belongsTo = Tag
    static constraints = {
    }
    enum TodoStatus {
        Pending('Pending'),
        Completed('Completed')

        final String value

        TodoStatus(String value) {
            this.value = value
        }

        String toString() {
            value
        }

        String getKey() {
            name()
        }
    }
}
