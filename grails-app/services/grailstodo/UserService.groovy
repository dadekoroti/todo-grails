package grailstodo

import com.security.auth.Role
import com.security.auth.User
import com.security.auth.UserRole
import grails.transaction.Transactional

@Transactional
class UserService {

    def serviceMethod() {

    }
    def initUser(){ //sabai file delete garda ni hunchha nachhahine agi maile vanya - minus tei ho


        Role.findByAuthority('ROLE_ADMIN') ?: new Role(authority: 'ROLE_ADMIN',).save(failOnError: true)

        def admin = User.findByUsername('admin') ?: new User(username: 'admin', password: 'admin', enabled: true,).save(failOnError: true)
        def role = Role.findByAuthority('ROLE_ADMIN')
        if (!admin.authorities.contains(role)) {
            UserRole.create admin, role
        }

        Role.findByAuthority('ROLE_USER') ?: new Role(authority: 'ROLE_USER',).save(failOnError: true)

        def user = User.findByUsername('user') ?: new User(username: 'user', password: 'user', enabled: true,).save(failOnError: true)
        def role2 = Role.findByAuthority('ROLE_USER')
        if (!user.authorities.contains(role)) {
            UserRole.create user, role2
        }
    }

//    def saveUser(params){
//        def newUser = new User(username: params.username, password: params.password, enabled: true).save(flush: true, failOnError: true)
//        def userRole = UserRole.create newUser, Role.findByAuthority('ROLE_USER')
//        if(newUser && userRole){
//           return newUser
//        }else{
//            return false
//        }
//    }


    def saveUser(User user){
        return user.save(flush:true)
    }

    /**
     *
     * @param user
     * @param role
     */
    def createNewUser(User user, Role role){
        println "in create new user "+user
        println "user.validate"+user.validate()
        if(user.validate()) {
            saveUser(user)
            if (!user.authorities.contains(role)) {
                UserRole.create(user, role)
                return user;


            }
        }
    }


    def updateUser(User user){
        if(user.validate()){
            saveUser(user)
        } else{
            user.errors?.allErrors?.each{
                throw new RuntimeException(messageSource.getMessage(it, null))
            }
        }
    }

    def saveUserInformation(UserInformation userInformation){
        return userInformation.save(flush: true)
    }
}

