package grailstodo

import com.security.auth.Role
import com.security.auth.User
import com.security.auth.UserRole
import grails.plugin.springsecurity.annotation.Secured

@Secured(['permitAll'])
class UserController {
def userService
    def springSecurityService


    def createUser(){
        def user = springSecurityService.currentUser
        println user
      [user:user]

    }

    def signUp(){
        println "======" + params
        def user = new User(params.username,params.password)
        println "======" + user
        def role = Role.findByAuthority(params.authority)
       def newUser = userService.createNewUser(user,role)
        if(newUser){

            def userInformation = new UserInformation()
            userInformation.user = newUser
            userInformation.name = params.name
            userInformation.address = params.address
            userInformation.dob = params.dob
            userInformation.gender = params.gender

           if( userService.saveUserInformation(userInformation)){

               flash.message = "save successfully"
               redirect( controller: 'home', action: 'dashboard')
           }else
               flash.message = "save failed"
        }


    }

    def login(){

    }


}
