package grailstodo

import grails.plugin.springsecurity.annotation.Secured

@Secured(['permitAll'])

class HomeController {
    def springSecurityService

    def index() { }

    def dashboard(){
        def user = springSecurityService.currentUser
        def userInformation = UserInformation.findByUser(user)
        [userInformation:userInformation]

    }
}
