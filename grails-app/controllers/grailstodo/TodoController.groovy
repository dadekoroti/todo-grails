package grailstodo

import grails.plugin.springsecurity.annotation.Secured
import groovy.sql.Sql


@Secured(['ROLE_ADMIN'])
class TodoController {


    def todoService
    def dataSource

    def index() {
       
        redirect(action: "home")
    }



    def home(){
        def completedTasks = Todo.findAll()

        [completedTasks:completedTasks]

    }

    def settings(){
        def tags = Tag.listOrderById(order: 'desc');
        def tasks = Todo.listOrderById(order: 'desc');
        [tags:tags, tasks:tasks]
    }

//    def profile(){
//        def user = User.findById(session['user_id'])
//        [user:user]
//    }
    def storeTodos(){
        def todo = new Todo()
        todo.note = params.task
        todo.created_at = new Date()
        todo.status = params.status
        todo.deadline = params.deadline
                // uta bata veifierd vaner pathaune hain tah parameter?


        // aba baau patta lagaune pahila
//        def tag= Tag.findById(params.id)
        // ab baau lai rakhne
//        todo.addToTag(tag);



//        render(todo.status.values())

        if(todo.save(flush: true)){
            flash.message = "successfully created"
            println flash.message
            redirect(action: "settings")
        }else{
            render("failed")
        }

    }

    def storeTags(){
        def tag = new Tag()
        def interest = new Interest()
//        def user = new User()
        tag.tagName = params.tagName
        interest.interest = params.tagName
        tag.created_at = new Date()

//        user = User.findById(session['user_id'])

//        interest.setUser(user)
//        interest.save(flush: true)
//        if(tag.save(flush: true)){
//
//            redirect(action: "settings")
//        }else{
//            render("failed")
//        }

    }

    def deleteTag(){
        def tag = Tag.findById(params.id)
        tag.delete(flush: true)
        redirect(action: "settings")
    }
    def deleteTodo(){
        def todo = Todo.findById(params.id)
        todo.delete(flush: true)
        redirect(action: "settings")
    }

    def assignTag(){
        def tagId = Tag.findById(params.tag_id)
        def taskId = Todo.findById(params.task_id)

        def sql = Sql.newInstance(dataSource)
        def query = sql.execute("INSERT INTO TAG_TODO (tag_id,todo_id) VALUES ('${tagId.id}','${taskId.id}');") // yaha chai real table ko name lekhne
        sql.close()

        // hora,, tei aayena,,  bauko address raklhda problem vako hola todo insert dekhau

        redirect(action: "settings")

    }
    def changeStatus(){
        def todo = Todo.get(params.todo_id)
        if (todo.status == Todo.TodoStatus.Pending){
            todo.status = Todo.TodoStatus.Completed
        }else {
            todo.status = Todo.TodoStatus.Pending
        }

        if(todo.save(flush: true)){
            flash.message = "successfully changed"// direct page ma redirect navai index action vako le kaam garena.
            redirect(action: "home")// seting page ma kamm gari ra xa. sajilai hola , maile badi time dina
        }else{
            render("failed")
        }
    }
}
