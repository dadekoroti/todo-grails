<%--
  Created by IntelliJ IDEA.
  User: SocialAvesDev1
  Date: 8/23/2017
  Time: 3:58 PM
--%>

<%@ page import="grailstodo.Todo" contentType="text/html;charset=UTF-8" %>
%{--<g:if test="${session['user_id']}">--}%
<meta name="layout" content="header">
<g:if test="${flash.message}">
    <div class="alert alert-success fade in">
        <button class="close" data-dismiss="alert">x</button>
        <i class="fa-fw fa fa-check">/</i>${flash.message}
    </div>
</g:if>


<div class="container-fluid">
    <div class="col-md-6">
        <div class="add-tag">
            <h2>Add Tags</h2>
            <g:form action="storeTags">
                <div class="form-group">
                    <label for="tag">Tag:</label>
                    <input type="text" class="form-control" id="tag" placeholder="Enter tag" name="tagName">
                </div>
            %{--<div class="form-group">--}%
            %{--<div class="input-group date" data-provide="datepicker">--}%
            %{--<input type="text" class="form-control">--}%
            %{--<div class="input-group-addon">--}%
            %{--<span class="glyphicon glyphicon-th"></span>--}%
            %{--</div>--}%
            %{--</div>--}%
            %{--</div>--}%

                <button type="submit" class="btn btn-default">Submit</button>
            </g:form>
        </div>

        <div class="show-tag">
            <table class="table table-striped" id="myTable1">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Tag</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${tags}" var="tag">
                    <tr>
                        <td>${tag.id}</td>
                        <td>${tag.tagName}</td>
                        <td><g:formatDate format="yyyy-MM-dd" date="${tag.created_at}"/></td>
                        <td>
                            <g:link controller="todo" action="deleteTag" id="${tag.id}"><button class="btn btn-danger">Delete</button> </g:link>

                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="add-tag">
            <h2>Add Task</h2>
            <g:form controller="todo" action="storeTodos">
                <div class="form-group">
                    <label for="task">Task:</label>
                    <input type="text" class="form-control" id="task" placeholder="Enter task" name="task" required>
                </div>
                <div class="form-group">
                    <g:datePicker  id="deadline" name="deadline" precision="day"></g:datePicker >
                </div>
                <div class="form-group">
                    <g:select name="status" from="${ grailstodo.Todo.TodoStatus.values()}" class="form-control" optionKey="key"/>
                </div>

            %{--<div class="form-group">--}%
            %{--<div class="input-group date" data-provide="datepicker">--}%
            %{--<input type="text" class="form-control">--}%
            %{--<div class="input-group-addon">--}%
            %{--<span class="glyphicon glyphicon-th"></span>--}%
            %{--</div>--}%
            %{--</div>--}%
            %{--</div>--}%
                <button type="submit" class="btn btn-default">Submit</button>
            </g:form>
        </div>

        <div class="show-tag">
            <table class="table table-striped" id="myTable2">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Task</th>
                    <th>Status</th>
                    <th>Remaining Days</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${tasks}" var="task">
                    <tr>
                        <td>${task.id}</td>
                        <td>${task.note}
                        <td>${task.status}

                            <div class="tags">
                                <ul>
                                    <g:each in="${task.tag}" var="tag">
                                        <li>${tag.tagName}</li>
                                    </g:each>
                                </ul>
                            </div>
                        </td>
                        <td><g:formatDate format="yyyy-MM-dd" date="${task.created_at}"/></td>
                        <td>

                            <g:form controller="todo" action="assignTag">
                                <input type="hidden"  value="${task.id}" name="task_id">
                                <select class="form-control assign-tag" name="tag_id">
                                    <option>Assign Tag</option>
                                    <g:each in="${tags}" var="tag">
                                        <option value="${tag.id}">${tag.tagName}</option>
                                    </g:each>
                                </select>
                            </g:form>
                            <g:link controller="todo" action="deleteTodo" id="${task.id}"><button class="btn btn-danger">Delete</button> </g:link>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $('.assign-tag').on('change',function(){
        var URL="${createLink(controller:'todo',action:'assignTag')}";
        var task_id = $(this).parent().find('input').val();
        var tag_id = $(this).val()
//        alert(task_id)
//        alert(tag_id)
//        $.ajax({
//            url:URL,
//            data: {id:'1'},
//            success: function(resp){
//                console.log(resp);
//                $("#author").val(resp.author);
//                $("#book").val(resp.bookName);
//            }
//        });
        $(this).parent().submit();
    });

    $(document).ready(function(){
        $('#myTable1').DataTable();
        $('#myTable2').DataTable();
    });
</script>
%{--</g:if>--}%
%{--<g:else>--}%
    %{--<g:link controller="todo" action="login">Login First</g:link>--}%
%{--</g:else>--}%