<%--
  Created by IntelliJ IDEA.
  User: SocialAvesDev1
  Date: 8/23/2017
  Time: 3:56 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
%{--<g:if test="${session['user_id']}">--}%
<meta name="layout" content="header">
<div class="container-fluid">
    <g:if test="${flash.message}">
        <div class="alert alert-success fade in">
            <button class="close" data-dismiss="alert">x</button>
            <i class="fa-fw fa fa-check"></i>${flash.message}
        </div>
    </g:if>
    <div class="title">Todo List</div>

    <div class="col-md-6 pending-task">
        <span class="headings">Pending</span>
        <div class="todo-list">

            <ul class="todo-ul">
            %{--${println(tasks)}--}%
                <g:each in="${completedTasks}" var="task">
                    <g:if test="${task.status == grailstodo.Todo.TodoStatus.Pending}">
                        <li>
                            <span class="task">${task.note}<span class="date pull-right"><g:formatDate format="yyyy-MM-dd" date="${task.created_at}"/></span></span><br/>
                            <g:each in="${task.tag}" var="tag">
                                <span class="tag">
                                    ${tag.tagName}
                                </span>
                            </g:each>
                            <div class="change-status hidden">
                                <p>Change Status</p>
                                <g:form class="change-status-form hidden" action="changeStatus">
                                    <input type="text" value="${task.id}" name="todo_id">
                                </g:form>
                            </div>
                        </li>
                    </g:if>
                </g:each>

            </ul>
        </div>
    </div>
    <div class="col-md-6 completed-task">
        <span class="headings">Completed</span>
        <div class="todo-list">
            <ul class="todo-ul">
            %{--${println(tasks)}--}%
                <g:each in="${completedTasks}" var="task">
                    <g:if test="${task.status == grailstodo.Todo.TodoStatus.Completed}">
                        <li>
                            <span class="task">${task.note}<span class="date pull-right"><g:formatDate format="yyyy-MM-dd" date="${task.created_at}"/></span></span><br/>
                            <g:each in="${task.tag}" var="tag">
                                <span class="tag">
                                    ${tag.tagName}
                                </span>
                            </g:each>
                            <div class="change-status hidden">
                                <p>Change Status</p>
                                <g:form class="change-status-form hidden" action="changeStatus">
                                    <input type="text" value="${task.id}" name="todo_id">
                                </g:form>
                            </div>
                        </li>
                    </g:if>
                </g:each>

            </ul>
        </div>
    </div>

</div>
<script>
    $('.todo-ul li').hover(function () {
        $(this).find('.change-status').removeClass('hidden');
    },function () {
        $(this).find('.change-status').addClass('hidden');
    })

    $('.change-status').click(function () {
        $(this).find('.change-status-form').submit()
    });
</script>
%{--</g:if>--}%
%{--<g:else>--}%
    %{--<g:link controller="todo" action="login">Login First</g:link>--}%
%{--</g:else>--}%