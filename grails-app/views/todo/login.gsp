<%--
  Created by IntelliJ IDEA.
  User: madhu-pc
  Date: 8/30/2017
  Time: 11:19 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<meta name="layout" content="header">
<style>
    #login-form{
        margin-top: 50px;
    }
</style>
<g:if test="${flash.messageError}">
    <div class="alert alert-danger fade in">
        <button class="close" data-dismiss="alert">x</button>
        <i class="fa-fw fa fa-check"></i>${flash.messageError}
    </div>
</g:if>
<div class="container">

    <div class="col-md-offset-3 col-md-6" id="login-form">
        <div id="exTab1">
            <ul  class="nav nav-pills">
                <li class="active">
                    <a  href="#1a" data-toggle="tab">Login</a>
                </li>
                <li><a href="#2a" data-toggle="tab">Register</a>

            </ul>
            <hr></hr>
            <div class="tab-content clearfix">
                <div class="tab-pane active" id="1a">
                    <g:form controller="todo" action="checkLogin">
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" name="email" id="email">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" name="psw" id="pwd">
                        </div>
                    %{--<div class="checkbox">--}%
                    %{--<label><input type="checkbox"> Remember me</label>--}%
                    %{--</div>--}%
                        <button type="submit" class="btn btn-default">Submit</button>
                    </g:form>
                </div>
                <div class="tab-pane" id="2a">
                    <g:form controller="todo" action="signUp">
                        <div class="form-group">
                            <label>Name:</label>
                            <input type="text" class="form-control" name="name" id="sign_up_name">
                        </div>
                        <div class="form-group">
                            <label>Email:</label>
                            <input type="email" class="form-control" name="email" id="sign_up_email">
                        </div>
                        <div class="form-group">
                            <label>Gender:</label>
                            <label class="radio-inline"><input type="radio" name="gender" value="male">Male</label>
                            <label class="radio-inline"><input type="radio" name="gender" value="female">Female</label>
                            <label class="radio-inline"><input type="radio" name="gender" value="other">Other</label>

                        </div>
                        <div class="form-group">
                            <label>Interests:</label>

                                <div class="checkbox">
                                    <label><input type="checkbox" name="interest" value="Sport">Sport</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="interest" value="Programming">Programming</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="interest" value="Gaming">Gaming</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="interest" value="Adventure">Adventure</label>
                                </div>


                        </div>
                        <div class="form-group">
                            <label>You are :</label>
                            <select class="form-control" name="info">
                                <option>Programmer</option>
                                <option>Student</option>
                                <option>Player</option>
                                <option>Student</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" name="sign_up_psw" id="sign_up_pwd">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Retype Password:</label>
                            <input type="password" class="form-control" name="retype_sign_up_psw" id="retype_sign_up_pwd">
                        </div>
                    %{--<div class="checkbox">--}%
                    %{--<label><input type="checkbox"> Remember me</label>--}%
                    %{--</div>--}%
                        <button type="submit" class="btn btn-default">Submit</button>
                    </g:form>
                </div>

            </div>
        </div>

    </div>
</div>

