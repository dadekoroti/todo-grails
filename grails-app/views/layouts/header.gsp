<%--
  Created by IntelliJ IDEA.
  User: madhu-pc
  Date: 8/23/2017
  Time: 10:28 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    %{--date picker css--}%
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.standalone.css" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="${resource(dir: 'stylesheets', file: 'costume.css')}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <g:layoutHead/>
</head>
<body>
%{--<g:if test="${session['user_id']}">--}%
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <g:link controller="todo" class="navbar-brand" action="home">METodo</g:link>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><g:link controller="todo" action="home">Home</g:link></li>
            <li><g:link controller="todo" action="settings">Settings</g:link></li>
            <li><g:link controller="todo" action="profile">Profile</g:link></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><g:link controller="todo" action="signUp"><span class="glyphicon glyphicon-user"></span> Sign Up </g:link></li>
            <li><g:link controller="todo" action="login"><span class="glyphicon glyphicon-log-in"></span> Login </g:link></li>
            <li><g:link controller="todo" action="logout"><span class="glyphicon glyphicon-log-out"></span> Logout </g:link></li>
        </ul>
    </div>
</nav>
%{--</g:if>--}%
<g:layoutBody/>
</body>
</html>

